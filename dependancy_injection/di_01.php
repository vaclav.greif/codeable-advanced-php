<?php

class A {
	/**
	 * @var \B
	 */
	private $b;

	public function __construct( B $b ) {
		$this->b = $b;
	}
}

class B {
	public function __construct() {
	}
}

new A( new B() );
