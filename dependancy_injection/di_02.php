<?php

class A {
	/**
	 * @var \B
	 */
	private $b;

	public function __construct( B $b ) {
		$this->b = $b;
	}
}

class B {
	public function __construct() {
	}
}

function create_with_b( $class ) {
	return new $class( new B );
}

create_with_b( '\A' );
