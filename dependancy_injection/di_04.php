<?php

use Dice\Dice;

require_once 'vendor/autoload.php';

class A {
	/**
	 * @var \B
	 */
	private $b;

	public function __construct( B $b ) {
		$this->b = $b;
	}
}

class B {
	public function __construct() {
	}
}

$dice = new Dice();
$dice = $dice->addRule( '\A', [ 'shared' => true ] );

$a1 = $dice->create( '\A' );
$a2 = $dice->create( '\A' );

var_dump( $a1 === $a2 );
