<?php

interface Expert {
	public function pitch();
}

abstract class ExpertBase implements Expert {
	public function estimate() {
		echo 'price is $60';
	}
}

class NathanReimnitz extends ExpertBase {

	public function pitch() {
		echo 'If these rates are aligned with your expectations I would be happy to continue this conversation with you.  Please let me know either way, thanks!';
	}
}

class DerrickHammer extends ExpertBase {

	public function pitch() {
		echo 'Please let me know if this is something you can invest in. Thanks :)';
	}
}

class Room {
	/**
	 * @var Expert[]
	 */
	private $experts = [];

	public function add_expert( Expert $expert ) {
		$this->experts[ get_class( $expert ) ] = $expert;
	}

	public function print_experts() {
		var_dump( $this->experts );
	}
	public function print_expert_pitches() {
		/** @var \ExpertBase $expert */
		foreach ($this->experts as $name => $expert){
			echo $name. ': '. $expert->pitch() ."\n";
		}
	}

}

$room = new Room();
$room->add_expert( new NathanReimnitz() );
$room->add_expert( new DerrickHammer() );

$room->print_experts();
$room->print_expert_pitches();
