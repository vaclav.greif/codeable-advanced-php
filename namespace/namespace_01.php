<?php
// No namespace, no prefix

add_action( 'init', 'codex_custom_init' );
function codex_custom_init() {
	$args = array(
		'public' => true,
		'label'  => 'Books',
	);
	register_post_type( 'book', $args );
}
