<?php
// No namespace, prefix

add_action( 'init', 'myprefix_codex_custom_init' );
function myprefix_codex_custom_init() {
	$args = array(
		'public' => true,
		'label'  => 'Books',
	);
	register_post_type( 'book', $args );
}
