<?php
// Namespace, function

namespace MySpecialPlugin;

add_action( 'init', __NAMESPACE__ . '\myprefix_codex_custom_init' );
function myprefix_codex_custom_init() {
	$args = array(
		'public' => true,
		'label'  => 'Books',
	);
	register_post_type( 'book', $args );
}
