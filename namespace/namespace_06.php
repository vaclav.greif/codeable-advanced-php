<?php
// namespace, use

namespace MySpecialPlugin;

use AnotherPackage\SomeClass;

class App {
	public function __construct() {
		add_action( 'init', [ $this, 'custom_init' ] );
	}

	public function custom_init() {
		$args = array(
			'public' => true,
			'label'  => 'Books',
		);
		register_post_type( 'book', $args );
	}

	public function test() {
		$something = new SomeClass();
	}

}