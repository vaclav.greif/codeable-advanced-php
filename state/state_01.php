<?php


abstract class Shape {
	const TYPE = 'nothing';

	public function get_type_base() {
		echo self::TYPE;
	}

	public function get_type() {
		echo static::TYPE;
	}
}

class Circle extends Shape {
	const TYPE = 'circle';
}

$circle = new Circle();
$circle->get_type_base();
$circle->get_type();
