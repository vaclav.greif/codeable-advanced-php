<?php


class Shape {
	private $type;

	public function get_type() {
		echo $this->type;
	}

	public function set_type() {
		$this->type = 'nothing';
	}
}

class Circle extends Shape {
	private $type;

	public function set_type2() {
		$this->type = 'circle';
	}

	public function get_type2() {
		echo $this->type;
	}
}

$circle = new Circle();
$circle->set_type();
$circle->get_type();
$circle->set_type2();
$circle->get_type2();
var_export( $circle );
