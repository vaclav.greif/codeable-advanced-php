<?php

trait state {
	private $data;

	/**
	 * @return mixed
	 */
	public function get_data() {
		return $this->data;
	}

	/**
	 * @param mixed $data
	 */
	public function set_data() {
		$this->data = mt_rand();
	}


}

class A {
	use state;
}

class B extends A {
	private $type = 'type';

	/**
	 * @return string
	 */
	public function get_type() {
		return $this->type;
	}
}

class C {
	private $type = 'type';

	/**
	 * @return string
	 */
	public function get_type() {
		return $this->type;
	}
}


$b = new B();
$b->set_data();
var_dump( $b->get_data() );
var_dump( $b );
var_export( $b );

$c = new C();
$c->get_type();
