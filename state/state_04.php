<?php


class Shape {
	private static $type;

	/**
	 * @return mixed
	 */
	public static function get_type() {
		return self::$type;
	}

	public static function get_type2() {
		return static::$type;
	}

	public static function get_type3() {
		$class = get_called_class();

		return $class::$type;
	}

	public static function set_type() {
		$class        = get_called_class();
		$_type        = strtolower( ( new \ReflectionClass( get_called_class() ) )->getShortName() );
		$class::$type = $_type;
	}
}

class Circle extends Shape {

}

class Square extends Shape {

}

class Triangle extends Shape {
	protected static $type;
}

var_dump( 'Circle');

Circle::set_type();
var_dump( 'Circle Circle');
var_dump( Circle::get_type() );
var_dump( Circle::get_type2() );
var_dump( Circle::get_type3() );

var_dump( 'Circle Square');
var_dump( Square::get_type() );
var_dump( Square::get_type2() );
var_dump( Square::get_type3() );

var_dump( 'Square');
Square::set_type();

var_dump( 'Square Circle');
var_dump( Circle::get_type() );
var_dump( Circle::get_type2() );
var_dump( Circle::get_type3() );

var_dump( 'Square Square');
var_dump( Square::get_type() );
var_dump( Square::get_type2() );
var_dump( Square::get_type3() );


var_dump( 'Triangle');
Triangle::set_type();

var_dump( 'Triangle Circle');
var_dump( Circle::get_type() );
var_dump( Circle::get_type2() );
var_dump( Circle::get_type3() );

var_dump( 'Triangle Square');
var_dump( Square::get_type() );
var_dump( Square::get_type2() );
var_dump( Square::get_type3() );

var_dump( 'Triangle Triangle');
var_dump( Triangle::get_type() );
var_dump( Triangle::get_type2() );
var_dump( Triangle::get_type3() );
