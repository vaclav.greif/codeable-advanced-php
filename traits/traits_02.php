<?php


trait hello {
	public function hello() {
		echo 'hello';
	}
}


class world {
	use hello;
	public function hello() {
		echo 'fellow';
	}
}

( new world() )->hello();
