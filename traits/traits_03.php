<?php


trait hello {
	public function hello() {
		echo 'hello';
	}
}

class universe {
	public function hello() {
		echo 'fellow';
	}
}

class world extends universe {
	use hello;
}

( new world() )->hello();
